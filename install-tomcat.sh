#/bin/bash

DEFAULT_TOMCAT_HOME=/opt/tomcat
DEFAULT_TOMCAT_USER=tomcat
DEFAULT_OPENDJ_HOME=/opt/opendj
DEFAULT_OPENDJ_USER=ldap
DEFAULT_IDP_HOME=/opt/idp
DEFAULT_PHIL_SSO_OXAUTH_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# helper functions
. "$DEFAULT_PHIL_SSO_OXAUTH_HOME/lib/common.sh"

### begin script ###
init_gluu_config

# copy tomcat files
TOMCAT_FILES[0]="$PHIL_SSO_OXAUTH_HOME/tomcat/"
DEST_DIR="$TOMCAT_HOME/"
rsync -av --progress "${TOMCAT_FILES[@]}" "$DEST_DIR"
chown -Rvf $TOMCAT_USER.$TOMCAT_USER "$DEST_DIR"

# spill idp.war into webapps directory instead of using warfile
# and add idp customizations
IDP_FILES[0]="$PHIL_SSO_OXAUTH_HOME/idp/"
IDPWAR="$IDP_HOME/war/idp.war"
IDPWEBAPP="$TOMCAT_HOME/webapps/idp/"
(
	mkdir -pv $IDPWEBAPP
	cd $IDPWEBAPP
	unzip $IDPWAR
)
rsync -av --progress "${IDP_FILES[@]}" "$IDPWEBAPP"
chown -Rvf $TOMCAT_USER.$TOMCAT_USER "$IDPWEBAPP"

# and disable gluu's localhost/idp.xml config which depends on the warfile
IDPXML="$TOMCAT_HOME/conf/Catalina/localhost/idp.xml"
if [ -f "$IDPXML" ]; then
	mv -v "$IDPXML" "$IDPXML".orig
fi

# restart tomcat
echo -n 'Tomcat needs to be restarted. Restart now? [Y/n] '
read TOMCAT_RESTART

if [ -z "$TOMCAT_RESTART" ]; then
	TOMCAT_RESTART=y
elif [ "$TOMCAT_RESTART" = "n" ]; then
	TOMCAT_RESTART=
fi

if [ "$TOMCAT_RESTART" ]; then
	gluu_restart_wait
	echo 'phil_sso_oxauth installed!'
else
	echo 'phil_sso_oxauth installed. You will need to restart tomcat to see the pki changes.'
fi
