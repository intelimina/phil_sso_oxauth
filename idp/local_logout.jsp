<%@page import="
edu.internet2.middleware.shibboleth.idp.util.HttpServletHelper,
edu.internet2.middleware.shibboleth.idp.session.Session,
edu.internet2.middleware.shibboleth.common.session.SessionManager" %><%

	/* delete the login context */
	HttpServletHelper.unbindLoginContext(HttpServletHelper.getStorageService(application), application, request, response);

	/* destroy the shibboleth session */
	Session s = (Session) request.getAttribute(Session.HTTP_SESSION_BINDING_ATTRIBUTE);
	if (s != null) {
		SessionManager sm = HttpServletHelper.getSessionManager(application);
		String id = s.getSessionID();
		sm.destroySession(id);
	}

	/* delete all cookies */
	Cookie [] ck = request.getCookies();
	if (ck != null) {
		for (int i=0; i < ck.length; i++) {
			Cookie cc = ck[i];
			cc.setMaxAge(0);
			response.addCookie(cc);
		}
	}

%><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>Shibboleth Logout</title>
</head>
<body>
You have been logged out of Shibboleth.

<script>
//<![CDATA[

// delete the session_state cookie
var delete_cookie = function(name) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

delete_cookie("session_state");
//]]>
</script>
</body>
</html>
