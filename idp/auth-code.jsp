<%@page import="
java.util.*,
javax.servlet.http.HttpSession
" %><%
/**
 * Ideally, you want to put your action scripting in a controller / action object,
 * however, we are minimizing patches / changes to the IDP code itself
 * so were doing routing logic here. - madumlao
 *
 * Abandon hope, ye who enter.
 *
 * modes:
 * fragment mode
 *   query parameters received as a fragment
 *   will redirect to self with query
 *
 * query mode
 *   query parameters received as query
 *   add session_state parameter to session data
 *
 **/

Map<String, String[]> params = request.getParameterMap();

String sessionState = request.getParameter("session_state");
HttpSession httpSession = request.getSession(true);
if (sessionState != null) {
	httpSession.setAttribute("sessionState", sessionState);
}
%><html>
<head>
    <script type="text/javascript" language="javascript">
        //<![CDATA[
        function generateQueryString()
        {
            var href = window.location.href;
            var queryString = "";

            var currentQueryString = "";
            var hashQueryString = "";
            if(href.indexOf('?') != -1){
                if(href.indexOf('#') != -1){
                    currentQueryString = href.slice(href.indexOf('?') + 1, href.indexOf('#'));
                } else {
                    currentQueryString = href.slice(href.indexOf('?') + 1);
                }
            }

            if(href.indexOf('#') != -1){
                hashQueryString = href.slice(href.indexOf('#') + 1);
            }

            if(currentQueryString != "" || hashQueryString != ""){
                if(currentQueryString != ""){
                    queryString = "?" + currentQueryString;
                    if(hashQueryString != ""){
                        queryString += "&" + hashQueryString;
                    }
                } else {
                    queryString = "?" + hashQueryString;
                }
            }

            return queryString;
        }

        function redirect(){
            <% if (params.size() > 0) { %>
            var loc = "<%=request.getContextPath() + "/Authn/RemoteUser"%>" + generateQueryString();
            <% } else { %>
            var loc = "<%=request.getContextPath() + "/auth-code.jsp"%>" + generateQueryString();
            <% } %>
            window.location = loc;
        }

        // ]]>
    </script>
</head>
<body onLoad="redirect()"></body>
</html>
