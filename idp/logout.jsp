<%@page import="
java.util.*,
java.io.*,
java.nio.charset.StandardCharsets,
java.net.URLEncoder,
java.util.zip.Deflater,
java.util.zip.DeflaterOutputStream,
java.util.zip.Inflater,
java.util.zip.InflaterOutputStream,

org.joda.time.DateTime,
org.w3c.dom.Document,
org.w3c.dom.Element,

org.apache.http.client.methods.HttpGet,
org.apache.http.HttpResponse,
org.apache.http.HttpEntity,
org.apache.http.impl.client.DefaultHttpClient,
org.apache.http.client.HttpClient,
edu.internet2.middleware.shibboleth.idp.authn.LoginContext,
edu.internet2.middleware.shibboleth.idp.util.HttpServletHelper,

org.opensaml.Configuration,
org.opensaml.common.SAMLObjectBuilder,
org.opensaml.saml2.core.LogoutRequest,
org.opensaml.saml2.core.LogoutResponse,
org.opensaml.saml2.core.Status,
org.opensaml.saml2.core.StatusCode,
org.opensaml.saml2.core.Issuer,
org.opensaml.saml2.core.NameID,
org.opensaml.saml2.binding.encoding.HTTPRedirectDeflateEncoder,
org.opensaml.saml2.metadata.EntityDescriptor,
org.opensaml.saml2.metadata.SPSSODescriptor,
org.opensaml.saml2.metadata.RoleDescriptor,
org.opensaml.saml2.metadata.SingleLogoutService,
org.opensaml.xml.util.Base64,
org.opensaml.xml.util.XMLHelper,
org.opensaml.xml.io.Marshaller,
org.opensaml.xml.io.MarshallerFactory,
org.opensaml.xml.io.Unmarshaller,
org.opensaml.xml.io.UnmarshallerFactory,
org.opensaml.xml.parse.BasicParserPool,
org.opensaml.xml.XMLObjectBuilderFactory,
org.opensaml.util.storage.StorageService,

edu.internet2.middleware.shibboleth.idp.session.Session,
edu.internet2.middleware.shibboleth.idp.session.ServiceInformation,
edu.internet2.middleware.shibboleth.idp.profile.saml2.SLOProfileHandler,
edu.internet2.middleware.shibboleth.common.session.SessionManager,
edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfigurationManager,
edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfiguration,
edu.internet2.middleware.shibboleth.common.relyingparty.ProfileConfiguration,

org.gluu.oxauth.client.session.OAuthData
" %><%
/**
 * Ideally, you want to put your action scripting in a controller / action object,
 * however, we are minimizing patches / changes to the IDP code itself
 * so we're doing routing logic here. - madumlao
 *
 * Abandon hope, ye who enter.
 *
 * modes:
 *  SAMLResponse mode
 *    SAMLResponse provided in params
 *    - parse the SAML response and either show OK pixel or 4xx error code
 *
 *  Logout mode
 *    SAML session present
 *    - locate all other SAML sessions and issue an SLO request to each using images
 *    - upon finished, redirect to OpenID logout
 *
 *  Logged out mode
 *    SAML session not present
 *    - redirect to OpenID logout
 **/

// http session
HttpSession httpSession = request.getSession();

// parse parameters
String samlResponse = request.getParameter("SAMLResponse");
String samlResponseIssuer = "";

// detect OpenID session / workflow
// if fromOIDC - no redirect, silent page
// else - redirect to OIDC logout url with post_logout_redirect_url param
String fromOIDC = request.getParameter("fromOIDC");
String redirectURL;
org.gluu.oxauth.client.util.Configuration ssoConfig = org.gluu.oxauth.client.util.Configuration.instance();
OAuthData oAuthData = (OAuthData)session.getValue(ssoConfig.SESSION_OAUTH_DATA);
if (oAuthData != null) {
	String postLogoutRedirectURL = request.getParameter("post_logout_redirect_uri");
	if (postLogoutRedirectURL == null) {
		postLogoutRedirectURL = request.getScheme() + "://" + request.getServerName() + "/identity/authentication/finishlogout";
	}

	// construct redirectUrl
	String idToken = oAuthData.getIdToken();
	String logoutUri = ssoConfig.getPropertyValue(ssoConfig.OAUTH_PROPERTY_LOGOUT_URL);
	redirectURL = logoutUri
		+ "?id_token_hint=" + idToken
		+ "&session_state=" + httpSession.getAttribute("sessionState")
		+ "&post_logout_redirect_uri=" + postLogoutRedirectURL;
} else {
	redirectURL = request.getScheme() + "://" + request.getServerName() + "/identity/authentication/finishlogout";
}

// protocol / binding support
String supportedLogoutProtocol = "urn:oasis:names:tc:SAML:2.0:protocol";
HTTPRedirectDeflateEncoder encoder = new HTTPRedirectDeflateEncoder();
String supportedBinding = encoder.getBindingURI();
	
// Session s = (Session) request.getAttribute(Session.HTTP_SESSION_BINDING_ATTRIBUTE);
Session s = HttpServletHelper.getUserSession(request);
RelyingPartyConfigurationManager rpcm = HttpServletHelper.getRelyingPartyConfigurationManager(application);
RelyingPartyConfiguration defparty = rpcm.getDefaultRelyingPartyConfiguration();

List<String> logoutUrls = new ArrayList<String>();
List<String> logoutSps = new ArrayList<String>();

if (samlResponse != null) {
/**
 * SAML Response mode
 * Parse the samlResponse and return an appropriate HTTP code
 *
 * 400 for bad SAML decode
 * 403 for valid decode but operation refused
 * 200 OK  otherwise
 */

	try {
		// step 1: decode saml data
		String samlData = "";
		byte[] paramDecoded = Base64.decode(samlResponse);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Inflater inflater = new Inflater(true);
		InflaterOutputStream inflaterOutputStream = new InflaterOutputStream(os, inflater);
		inflaterOutputStream.write(paramDecoded);
		inflaterOutputStream.close();
		os.close();

		samlData = os.toString();

		// step 2: parse saml data status
		BasicParserPool parserPool = new BasicParserPool();
		parserPool.setNamespaceAware(true);

		ByteArrayInputStream is = new ByteArrayInputStream(samlData.getBytes(StandardCharsets.UTF_8));
		Document messageDoc = parserPool.parse(is);
		Element messageElem = messageDoc.getDocumentElement();

		UnmarshallerFactory umf = Configuration.getUnmarshallerFactory();
		Unmarshaller um = umf.getUnmarshaller(messageElem);

		LogoutResponse resp = (LogoutResponse) um.unmarshall(messageElem);
		Status samlStatus = resp.getStatus();
		Issuer issuer = resp.getIssuer();
		samlResponseIssuer = issuer.getValue();

		if (samlStatus == null) {
			response.sendError(400, "Could not retrieve SAML Status");
		} else {
			StatusCode statusCode = samlStatus.getStatusCode();
			if (statusCode == null) {
				response.sendError(400, "Could not retrieve SAML Status Code");
			} else if (statusCode.getValue().equals(StatusCode.SUCCESS_URI) ||
			           statusCode.getValue().equals(StatusCode.UNKNOWN_PRINCIPAL_URI)) {
				// allow success / logged out user through
			} else { 
				response.sendError(403, statusCode.getValue());
			}
		}
	} catch (Exception e) {
		response.sendError(400, "Error decoding SAML response");
	}

	// all good, produce an invisible img instead of output
	response.sendRedirect(request.getContextPath() + "/images/ok.png");

} else if (s != null) {
/**
 * Create a list of LogoutRequests ready to be loaded later in the doc
 * then redirect to OpenID connect logout when done
 */
	for (ServiceInformation info : s.getServicesInformation().values()) {
		String entityId = info.getEntityID();
		EntityDescriptor metadata = HttpServletHelper.getRelyingPartyMetadata(entityId, rpcm);
		List<RoleDescriptor> roleDescriptors = metadata.getRoleDescriptors();
		SPSSODescriptor spDescriptor = metadata.getSPSSODescriptor(supportedLogoutProtocol);

		// find the first logout service with supported binding
		SingleLogoutService singleLogoutService = null;
		if (spDescriptor != null) {
			List<SingleLogoutService> logoutServices = spDescriptor.getSingleLogoutServices();
			for (SingleLogoutService logoutService : logoutServices) {
				if (logoutService.getBinding().equals(supportedBinding)) {
					singleLogoutService = logoutService;
					break;
				}
			}
		}

		// logout service was found, construct a logout request
		if (singleLogoutService != null) {

			// try to build a logoutrequest
			XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
			SAMLObjectBuilder<LogoutRequest> logoutRequestBuilder = (SAMLObjectBuilder<LogoutRequest>) builderFactory.getBuilder(LogoutRequest.DEFAULT_ELEMENT_NAME);
			LogoutRequest req = logoutRequestBuilder.buildObject();

			// populate the entity
			req.setID(info.getNameIdentifier());

			// issue instant
			req.setIssueInstant(DateTime.now());

			// reason
			req.setReason("Single Logout");
			req.setDestination(singleLogoutService.getLocation());

			// issuer
			SAMLObjectBuilder<Issuer> issuerBuilder = (SAMLObjectBuilder<Issuer>) builderFactory.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
			Issuer issuer = issuerBuilder.buildObject();
			issuer.setValue(defparty.getProviderId());
			req.setIssuer(issuer);

			// nameID
			SAMLObjectBuilder<NameID> nameIDBuilder = (SAMLObjectBuilder<NameID>) builderFactory.getBuilder(NameID.DEFAULT_ELEMENT_NAME);
			NameID nameID = nameIDBuilder.buildObject();
			nameID.setNameQualifier(defparty.getProviderId());
			nameID.setSPNameQualifier(entityId);
			nameID.setFormat(info.getNameIdentifierFormat());
			// nameID.setValue(s.getSessionID());
			nameID.setValue(info.getNameIdentifier());
			req.setNameID(nameID);	

			// request built, generate a logout URI for it
			MarshallerFactory mf = Configuration.getMarshallerFactory();
			Marshaller m = mf.getMarshaller(req);
			Element elem = m.marshall(req);

			String nodeString = XMLHelper.nodeToString(elem);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
			DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(os, deflater);
			deflaterOutputStream.write(nodeString.getBytes(StandardCharsets.UTF_8));
			deflaterOutputStream.close();
			os.close();

			String base64 = Base64.encodeBytes(os.toByteArray() );
			String requestParam = URLEncoder.encode(base64, "UTF-8");
			String logoutUrl = req.getDestination() + "?SAMLRequest=" + requestParam;
			logoutUrls.add(logoutUrl);
			logoutSps.add(entityId);
		}
	}
}
%><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>IDP Logout</title>
	<style>
		iframe { display: none; }
	</style>
</head>
<body>
<% if (samlResponse != null) { %>
<p>Successfully logged out of <%= samlResponseIssuer %></p>
<% } else if (s != null) { %>
<script>
// <![CDATA[
	// whether to redirect or not
	doRedirect = <%= fromOIDC == null ? true : false %>;

	successCounter = (function() {
		var logged_in_sessions_count = <%= logoutUrls.size() %>;
		return function(url) {
			logged_in_sessions_count--;
			console.log("Logged out of " + url);
			if (logged_in_sessions_count <= 0) {
				console.log("Logged out of all sessions");
				var local_logout_iframe = document.getElementById('local_logout');
				local_logout_iframe.onload = redirectOauth;
				local_logout_iframe.src = "<%= request.getContextPath() %>/local_logout.jsp";
			}
		}
	})();

	failCounter = function(url) {
		alert("Failed to logout of " + url);
	};

	redirectOauth = function() {
		if (doRedirect) {
			console.log("Redirecting to Oauth logout");
			showRedirectLink();
			window.location = "<%= redirectURL %>";
		} else {
			console.log("Not redirecting");
		}
	};

	hideRedirectLink = function() {
		var redirectLink=document.getElementById("redirect_link");
		redirectLink.style.display = "none";
	};

	showRedirectLink = function() {
		var redirectLink=document.getElementById("redirect_link");
		redirectLink.style.display = "";
	}
// ]]>
</script>

<%	for (int n = 0; n < logoutUrls.size(); n++) {
	/* load each logoutUrl as an img src to bypass iframe/script security */
	String logoutUrl = logoutUrls.get(n);
	String spUrl = logoutSps.get(n);
%>
	<img style="display:none" src="<%= logoutUrl %>" onload="successCounter('<%= spUrl %>')" onerror="failCounter('<%= spUrl %>')" />
<%	} %>
<iframe id="local_logout"></iframe>
<%	if (fromOIDC == null) { %>
<p id="redirect_link" onload="hideRedirectLink()"><a href="<%= redirectURL %>">Click here</a> if your browser does not redirect you.</p>
<%	} %>

<% } else {

	if (fromOIDC == null) {
		/* Already logged out at shibboleth, show "you have been logged out" page. */ %>
		<p><a href="<%= redirectURL %>">Click here</a> if your browser does not redirect you.</p>
		<iframe id="local_logout" src="<%= request.getContextPath() %>/local_logout.jsp" onload="window.location='<%= redirectURL %>'"></iframe>
<%	} else { %>
	<iframe id="local_logout" src="<%= request.getContextPath() %>/local_logout.jsp"></iframe>
<%	} %>
<% } %>
</body>
</html>
