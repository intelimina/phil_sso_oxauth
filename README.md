This project contains Gluu server customizations for Philippine SSO. Tested from version 2.4.2 to 2.4.4sp3

# Customizations
## Login Page
Login page has been updated with ICTO logos.

## opiframe
check session iframe page (opiframe) has been updated for compatiblity with older OIDC clients.

check session iframe page does not reference external JS.

## oxTrust session management
OpenID Connect specifies a session management tracking mechanism through the use of browser iframes. Support for session management is _optional_ and was not included as part of oxTrust. Session management iframes were added to oxTrust as outlined [in this upstream pull request](https://github.com/GluuFederation/oxTrust/pull/500). This allows oxTrust to automatically logout if the session was ended.

## SAML SingleLogout
SAML-based single logout is not officially supported by either [Gluu][gluu-SAML-SLO-issues] or [Shibboleth][shibboleth-SAML-SLO-issues] because SAML-based SLO faces many instabilities. A "best effort SAML SLO" was implemented that makes the user's browser call as many SAML client SLO logout pages as it can. This only works for SAML clients that have a SingleLogout profile supporting the HTTP Redirect binding.

[gluu-saml-SLO-issues]: https://gluu.org/docs/ce/operation/logout/
[shibboleth-SAML-SLO-issues]: https://wiki.shibboleth.net/confluence/display/CONCEPT/SLOIssues 

# Installation
## Gluu Tomcat server
### Production Install
Checkout this repository into a folder inside the Gluu chroot, for example, `/opt/phil_sso_oxauth`.

~~~
sudo service gluu-server-2.4.2 login
git clone https://bitbucket.org/intelimina/phil_sso_oxauth
~~~

From inside the chroot, run the install-tomcat.sh script
~~~
$ cd /opt/phil_sso_oxauth
$ ./install-tomcat.sh
phil_sso_oxauth installation directory? [/opt/phil_sso_oxauth]: 
Tomcat installation directory? [/opt/tomcat]: 
...
Tomcat needs to be restarted. Restart now? [Y/n] 
Welcome to the Gluu Server!
Stopping Tomcat Servlet Container...
Stopped Tomcat Servlet Container.
Starting Tomcat Servlet Container...
Waiting for Tomcat Servlet Container......
running: PID:13418
Waiting for server to be ready................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................done.
phil_sso_oxauth installed! You can now enable the script in your gluu interface.
~~~

At this point your Gluu instance should have the updated pages.

### Manual Install
This section describes what the install-tomcat.sh script does. You may safely skip this step if you have already run the installer above.

The install script does the following to your gluu server.

1. copies the contents of the `tomcat/` directory to your Gluu tomcat.
2. restarts tomcat

The tomcat/ directory has `.orig` files of anything it overwrites. In case you want to uninstall the files, simply look for any `.orig` files in the oxauth webapps directory and copy them over. For example:

~~~
cd /opt/tomcat/webapps/oxauth
cp opiframe.xhtml.orig opiframe.xhtml
~~~
