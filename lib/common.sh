# helper functions

# populates environment variables for tomcat etc
init_gluu_config() {
	# find installation dir
	echo -n 'phil_sso_oxauth installation directory? ['"$DEFAULT_PHIL_SSO_OXAUTH_HOME"']: '
	read PHIL_SSO_OXAUTH_HOME

	if [ -z "$PHIL_SSO_OXAUTH_HOME" ]; then
		PHIL_SSO_OXAUTH_HOME="$DEFAULT_PHIL_SSO_OXAUTH_HOME"
	fi

	if ! [ -d "$PHIL_SSO_OXAUTH_HOME" ]; then
		echo "Unable to open $PHIL_SSO_OXAUTH_HOME"
		exit 1
	fi

	# find tomcat dir
	echo -n 'Tomcat installation directory? ['"$DEFAULT_TOMCAT_HOME"']: '
	read TOMCAT_HOME

	if [ -z "$TOMCAT_HOME" ]; then
		TOMCAT_HOME="$DEFAULT_TOMCAT_HOME"
	fi

	if ! [ -d "$TOMCAT_HOME" ]; then
		echo "Unable to open $TOMCAT_HOME"
		exit 1
	fi

	# determine tomcat user
	echo -n 'Tomcat user? ['"$DEFAULT_TOMCAT_USER"']: '
	read TOMCAT_USER

	if [ -z "$TOMCAT_USER" ]; then
		TOMCAT_USER="$DEFAULT_TOMCAT_USER"
	fi

	if [ -z "$TOMCAT_USER" ]; then
		echo "Tomcat user not specified"
		exit 1
	fi

	# find shibboleth dir
	echo -n 'Shibboleth installation directory? ['"$DEFAULT_IDP_HOME"']: '
	read IDP_HOME

	if [ -z "$IDP_HOME" ]; then
		IDP_HOME="$DEFAULT_IDP_HOME"
	fi

	if ! [ -d "$IDP_HOME" ]; then
		echo "Unable to open $IDP_HOME"
		exit 1
	fi

	if ! [ -f "$IDP_HOME/war/idp.war" ]; then
		echo "Unable to open $IDP_HOME/war/idp.war"
		exit 1
	fi
}

# restarts tomcat and waits for it to be ready
gluu_restart_wait() {
	service tomcat restart
	echo -ne "Waiting for server to be ready"
	tail -n0 -F "$TOMCAT_HOME/logs/wrapper.log" 2>&1 | \
	while read LINE; do
		echo -ne .
		echo $LINE | egrep -q 'Server startup in [0-9]* ms' && pkill -P $$ tail
	done
	echo done.
}
