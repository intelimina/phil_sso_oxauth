var Login = function () {
    
    return {
        //main function to initiate the module
        init: function () {

            $.backstretch([
                        "login/assets/img/bg/1.jpg"
		        ], {
		          fade: 1000,
		          duration: 8000*5
		    });
	       
        }

    };

}();
